FROM mysql:latest
EXPOSE 3306
COPY ./shippers.sql /docker-entrypoint-initdb.d
